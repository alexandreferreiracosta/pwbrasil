<!DOCTYPE html>
<?php
try {
    $dirLog = dirname(__FILE__) . "/logConsultaTitulos.txt";
    $fp = fopen($dirLog, "a");

    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {

        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {

        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {

        $ip = $_SERVER['REMOTE_ADDR'];
    }

    $escreve = fwrite($fp, date("Y-m-d") . " | " . date("H:i:s") . " | " . $_SERVER['PHP_SELF'] . " | " . $ip . " | \n");
    fclose($fp);
} catch (Exception $e) {
    print "<script> alert('Erro: L.A.') </script>";
}
?>
<html lang="PT">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800italic,800,700italic,700,600italic,400italic,600,300italic,300|Oswald:400,300,700' rel='stylesheet' type='text/css'>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/owl.carousel.css" rel="stylesheet">
        <link href="css/owl.theme.css" rel="stylesheet">
        <link href="css/owl.transitions.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDsF5ohmSWG7i7ZMD_Sra2_ycSk9kNBhdk&callback"></script>
        <script type="text/javascript" src="baguetteBox.min.js"></script>
        <script src="js/mapa.js"></script>
        <script src="js/index.js"></script>
        <link rel="stylesheet" href="gallery-clean.css">

        <title>PW Brasil Export S/A</title>
        <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body data-spy="scroll" data-target=".main-nav">
        <header class="st-navbar" style="font-family: 'Oswald';">
            <nav class="navbar navbar-default navbar-fixed-top clearfix" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sept-main-nav">
                            <span class="sr-only">PW BRASIL</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.php"><img id="lg" class="img-responsive"></a>
                    </div>
                    <div class="collapse navbar-collapse main-nav" id="sept-main-nav">
                        <ul class="nav navbar-nav navbar-right" id="texto">
                            <li><a href="#quemsomos"><b>QUEM SOMOS</b></a></li>
                            <li><a href="#instalacoes"><b>INSTALAÇÕES</b></a></li>
                            <li><a href="#nossasmarcas"><b>NOSSAS MARCAS</b></a></li>
                            <li><a href="#nossaslojas"><b>NOSSAS LOJAS</b></a></li>
                            <li><a href="#trabalheconosco"><b>TRABALHE CONOSCO</b></a></li>
                            <li><a href="#contato"><b>CONTATO</b></a></li>

                        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <section class="home" id="home" data-stellar-background-ratio="0.4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="st-home-unit">
                            <div class="hero-txt">
                                <img class="img-responsive" src="imagem/logo_pw_home.png" />
                                <br>
                                <div class="col-md-6 "><img src="imagem/logo_videbula_home.png"></button></div>
                                <div class="col-md-6 "><img src="imagem/logo_missbella_home.png"></button></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="about" id="quemsomos">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title1 st-center">
                            <p>Somos uma empresa de Sucesso</p>
                        </div>
                        <div class="row mb90">
                            <div class="col-md-6" style="color:#000000;">
                                <p>
                                    A PW Brasil Export S/A possui uma das mais avançadas edificações, em layout,
                                    já construídas para a indústria do vestuário no país e no exterior, 
                                    respeitando os conceitos de qualidade, eficiência e produtividade, 
                                    buscando ao máximo, o bem estar de seus funcionários.
                                    São 14 mil metros quadrados de área construída coberta num terreno de 60 mil metros 
                                    quadrados que consumiram investimentos iniciais da ordem de 18 milhões de reais.
                                </p>
                                <p>Operando desde junho de 2003 em suas novas instalações,
                                    a PW Brasil Export S/A é um grupo empreendedor e de vanguarda, 
                                    que acredita na moda, no seu trabalho e no de seus funcionários, 
                                    por isso investe na qualidade de vida de sua equipe, fornecendo uma série de benefícios sociais,
                                    treinamento sistemático, programas motivacionais e de auxílio à comunidade.</p>
                            </div>
                            <div class="col-md-6">
                                <CENTER><img src="imagem/img_quemsomos.jpg" alt="" class="img-responsive"></CENTER>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- ITÉM INSTALAÇÕES DA PAGINA -->
        <section class="portfolio" id="instalacoes">
            <div class="container-fluid ">
                <div class="row">
                    <div class="col-md-12 no-padding ">
                        <div class="section-title2 st-center">
                            <p>Conheça um pouco das nossas instalações</p>
                        </div>
                        <div class="filter mb40">
                            <form id="filter">
                                <fieldset class="group">
                                    <label class="btn btn-default btn-main"><input type="radio" name="filter" value="photography">Unidade Colatina</label>
                                    <label class="btn btn-default"><input type="radio" name="filter" value="design">Unidade Baixo Guandu</label>
                                </fieldset>
                            </form>
                        </div>

                        <div class="grid">
                            <figure class="portfolio-item" data-groups='["photography"]' class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                <img src="imagem/almoxarifado_instalacao_col.jpg" alt=""/>
                            </figure>
                            <figure class="portfolio-item" data-groups='["design"]' class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                <img src="imagem/externa_instalacao_bg.jpg" alt=""/>
                            </figure>
                            <figure class="portfolio-item" data-groups='["design"]' class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                <img src="imagem/cima_instalacao_bg.jpg" alt=""/>
                            </figure>
                            <figure class="portfolio-item" data-groups='["photography"]' class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                <img src="imagem/corte_instalacao_col.jpg" alt=""/>
                            </figure>
                            <figure class="portfolio-item" data-groups='["design"]' class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                <img src="imagem/externa2_instalacao_bg.jpg" alt=""/>
                            </figure>
                            <figure class="portfolio-item" data-groups='["photography"]' class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                <img src="imagem/costura_instalacao_col.jpg" alt=""/>
                            </figure>
                            <figure class="portfolio-item" data-groups='["photography"]' class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                <img src="imagem/costura2_instalacao_col.JPG" alt=""/>
                            </figure>
                            <figure class="portfolio-item" data-groups='["photography"]' class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                <img src="imagem/externa_instalacao_col.jpg" alt=""/>
                            </figure>
                            <figure class="portfolio-item" data-groups='["photography"]' class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                <img src="imagem/externa2_instalacao_col.jpg" alt=""/>
                            </figure>
                            <figure class="portfolio-item" data-groups='["photography"]' class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                <img src="imagem/externa3_instalacao_col.jpg" alt=""/>
                            </figure>
                            <figure class="portfolio-item" data-groups='["photography"]' class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                <img src="imagem/externa4_instalacao_col.jpg" alt=""/>
                            </figure>
                            <figure class="portfolio-item" data-groups='["photography"]' class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                <img src="imagem/externa5_instalacao_col.jpg" alt=""/>
                            </figure>
                            <figure class="portfolio-item" data-groups='["photography"]' class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                <img src="imagem/externa6_instalacao_col.jpg" alt=""/>
                            </figure>
                            <figure class="portfolio-item" data-groups='["photography"]' class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                <img src="imagem/externa7_instalacao_col.jpg" alt=""/>
                            </figure>
                            <figure class="portfolio-item" data-groups='["photography"]' class="thumbnail" data-toggle="modal" data-target="#lightbox">
                                <img src="imagem/lavanderia_instalacao_col.jpg" alt=""/>
                            </figure>

                            <div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <img src="" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <br>
        <br>
        <br>
        <br>
        <section class="" id="nossasmarcas">
            <div class="section-title5 st-center">
                <p>Conheça nossas marcas</p>
            </div>
            <div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel">
                <div class="overlay"></div>
                <ol class="carousel-indicators">
                    <li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#bs-carousel" data-slide-to="1"></li>
                    <li data-target="#bs-carousel" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item slides active">
                        <div class="slide-1"></div>
                        <div class="hero">
                            <hgroup>
                                <img src="imagem/logo_videbula_marca.png" alt="">       
                                <h3 style="color:#ffffff;">Com mais de 30 anos de história, a Vide Bula surgiu no mercado brasileiro no início dos anos 80
                                    e rapidamente tornou-se referência principalmente no seguimento jeans wear.</h3>
                            </hgroup>
                            <a href="https://www.facebook.com/videbulaoficial/" target="_blank"><button class="btn btn-hero btn-lg" role="button"><i class="fa fa-facebook" style="margin-right: 7px;"></i>Curtir</button></a>
                            <a href="https://www.instagram.com/videbulaoficial/" target="_blank"><button class="btn btn-hero btn-lg" role="button" style="background-color:#cd486b;border-color:#cd486b;"><i class="fa fa-instagram" style="margin-right: 7px;"></i>Seguir</button></a>

                        </div>
                    </div>
                    <div class="item slides">
                        <div class="slide-2"></div>
                        <div class="hero">        
                            <hgroup>
                                <img src="imagem/logo_missbella_marca.png" alt="">     
                                <h3 style="color:#ffffff;">Desde 1999 a Missbella encanta seu público jovem e irreverente com coleções coloridas e divertidas, que une estilo e design contemporâneo.
                                    Através de suas estampas conta a história feliz de
                                    uma marca que cresce a cada dia e cativa mais e
                                    mais pessoas com sua autenticidade e originalidade.</h3>
                            </hgroup>
                            <a href="https://www.facebook.com/missbellaoficial/" target="_blank"><button class="btn btn-hero btn-lg" role="button"><i class="fa fa-facebook" style="margin-right: 7px;"></i>Curtir</button></a>
                            <a href="https://www.instagram.com/missbellaoficial/" target="_blank"><button class="btn btn-hero btn-lg" role="button" style="background-color:#cd486b;border-color:#cd486b;"><i class="fa fa-instagram" style="margin-right: 7px;"></i>Seguir</button><a/>

                        </div>
                    </div>
                    <div class="item slides">
                        <div class="slide-3"></div>
                        <div class="hero">        
                            <hgroup>
                                <img src="imagem/logo_videbulajr_marca.png" alt="">
                                <h3 style="color:#ffffff;">A videbula teen & kids traz em sua essência a atitude urbana e moderna, sem esquecer
                                    o lado lúdico e sonhador do seu público, combinando peças leves, 
                                    criativas e cheias de personalidade.</h3>
                            </hgroup>
                            <a href="https://www.facebook.com/videbulaoficial/" target="_blank"> <button class="btn btn-hero btn-lg" role="button"><i class="fa fa-facebook" style="margin-right: 7px;"></i>Curtir</button>
                                <a href="https://www.instagram.com/videbulaoficial/" target="_blank"><button class="btn btn-hero btn-lg" role="button" style="background-color:#cd486b;border-color:#cd486b;"><i class="fa fa-instagram" style="margin-right: 7px;"></i>Seguir</button></a>
                        </div>
                    </div>
                </div> 
            </div>
        </section>
        
        <br>
        <br>
        <br>
        <br>
        <br>
        
        <section class="features-desc" id="nossaslojas">
            <div id="servicewrap">
                <div class="container" style="text-align:center;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title4 st-center">
                                <p>ENCONTRE A LOJA MAIS PERTO DE VOCÊ</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="map" >
                            </div>
                        </div>

                        <div class="row" style="font-family: Oswald;">
                            <div class="col-lg-3">
                                <h4>Loja Virtual Missbella</h4>
                                <a style="outline:none" href="http://www.missbella.com.br" target="new"><button type="button" class="btn btn-primary">Acessar</button></a><br>
                            </div>
                            <div class="col-lg-3">
                                <h4>Loja Virtual Missbella Atacado</h4>
                                <a style="outline:none" href="http://atacado.missbella.com.br" target="new"><button type="button" class="btn btn-primary">Acessar</button></a><br>
                            </div>
                            <div class="col-lg-3">
                                <h4>Loja Virtual VideBula</h4>
                                <a href="http://www.videbula.com.br" target="new"><button type="button" class="btn btn-primary">Acessar</button></a><br>
                            </div>
                            <div class="col-lg-3">
                                <h4>Loja Virtual VideBula Atacado</h4>
                                <a href="http://atacado.videbula.com.br" target="new"><button type="button" class="btn btn-primary">Acessar</button></a><br>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <br>
            <br>
            <br>
            <br>

            <section class="call-2-acction"  id="trabalheconosco">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="c2a">
                                <h2>Trabalhe Conosco</h2>
                                <p>A PW BRASIL é uma empresa que atua de forma inovadora no Brasil. Buscamos profissionais dinâmicos e aptos para contribuir com criatividade e motivação no mercado de vestuário. Consideramos nossos colaboradores e funcionários como nosso maior patrimônio e procuramos investir ativamente em seu crescimento.</p>
                                <p>O objetivo desta seção é manter um banco com dados de profissionais de diversas áreas.</p>
                                <a href="php/trabalheconosco.php" class="btn btn-main btn-lg">Enviar Curriculo</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="contact" id="contato">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title6 st-center">
                                <p>Entre em contato conosco</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <form role="form" action="" id="mensagem" style="color:#000000;">
                                <input type="text" class="form-control" id="fname" name="fname" placeholder="Nome Completo" required="">
                                <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" required="">
                                <input type="text" class="form-control" id="subj" name="subj" placeholder="Assunto" required="">
                                <textarea id="mssg" name="mssg" placeholder="Mensagem" class="form-control" rows="10" required=""></textarea>
                                <button class="btn btn-main btn" type="submit" id="send" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Sending..."><i class="fa fa-paper-plane "></i> Enviar</button>
                            </form>
                            <div id="caixaTexto" style="color:#0971b2"></div>
                        </div>
                        <div class="col-md-6" style="color:#000000;">
                            <address>
                                Av. Fidelis Ferrari, 112 - Castelo Branco<br>
                                Colatina - ES,Brasil.<br>
                                Tel:(27) 2102-7450
                            </address>
                        </div>
                    </div>
                </div>
            </section>

            <footer class="site-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12" style="color:#000000;">
                            &copy;PW BRASIL EXPORT S/A 2018.
                        </div>
                    </div>
                </div>
            </footer>

            <script type="text/javascript">
                $("#mensagem").submit(function () {
                    var fname = $('#fname').val();
                    var email = $('#email').val();
                    var subj = $('#subj').val();
                    var mssg = $('#mssg').val();

                    $('#fname').val('');
                    $('#email').val('');
                    $('#subj').val('');
                    $('#mssg').val('');

                    $.post("php/enviaEmail.php",
                            {fname: fname, email: email, subj: subj, mssg: mssg},
                            function (data, status) {
                                if (status == "success") {
                                    $("#caixaTexto").html("Mensagem enviado com sucesso!!");
                                } else {
                                    $("#caixaTexto").html("Erro ao enviar o email!! Tentar novamente");
                                }
                            }
                    );
                    return false;
                });
            </script>

            <script>
                setTimeout(function () {
                    var msg = document.getElementById("#caixaTexto");
                    msg.parentNode.removeChild(msg);
                }, 300);
            </script>
            <script src="js/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/jquery.easing.min.js"></script>
            <script src="js/jquery.stellar.js"></script>
            <script src="js/jquery.appear.js"></script>
            <script src="js/jquery.nicescroll.min.js"></script>
            <script src="js/jquery.countTo.js"></script>
            <script src="js/jquery.shuffle.modernizr.js"></script>
            <script src="js/jquery.shuffle.js"></script>
            <script src="js/owl.carousel.js"></script>
            <script src="js/jquery.ajaxchimp.min.js"></script>
            <script src="js/script.js"></script>
    </body>
</html>
