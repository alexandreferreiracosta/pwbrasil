var map;
var infoWindow;

var markersData = [
    /*MISSBELLA VITORIA*/
    {
        lat: -20.3128848,
        lng: -40.2872362,
        nome: "Loja Missbella Shopping Vitória",
        morada1: "Av. Américo Buaiz, 200",
        morada2: "Enseada do Suá",
        codPostal: "29050-902 Vitória - ES",
        tel: "(27) 3319-8558",
        icon: "imagem/logo_localizacao_missbella_mapa.png",
        caminho: "imagem/miss.png"
    },
    /*missbella vila velha
     {
     lat: -20.3422568,
     lng: -40.2887315,
     nome: "Loja Missbella Shopping Praia da Costa",
     morada1: "Av. Doutor Olívio Lira, 353",
     morada2: "Praia da Costa",
     codPostal: "29055-460 Vila Velha - ES",
     tel: "(27) 3319-8558",
     icon: "imagem/minibela.png"
     },*/
    /*missbella BH*/
    {
        lat: -19.9342243,
        lng: -43.9407355,
        nome: "Loja Missbella Belo Horizonte",
        morada1: "R. Benvinda de Carvalho, 60",
        morada2: "Santo Antônio",
        codPostal: "30330-180 Belo Horizonte - MG",
        tel: "(31) 99174-3604",
        icon: "imagem/logo_localizacao_missbella_mapa.png",
        caminho: "imagem/miss.png"
    },
    /*missbella Recife*/
    {
        lat: -8.1172195,
        lng: -34.971411,
        nome: "Loja Missbella Recife",
        morada1: "R. Padre Carapuceiro, 777",
        morada2: "Boa Viagem",
        codPostal: "51020-280 Recife - PE",
        tel: "(81) 3328-7498",
        icon: "imagem/logo_localizacao_missbella_mapa.png",
        caminho: "imagem/miss.png"
    }
    ,
    /*videbula e missbela vila velha*/
    {
        lat: -20.3422568,
        lng: -40.2887315,
        nome: "Loja Missbella e Videbula Shopping Praia da Costa",
        morada1: "Av. Doutor Olívio Lira, 353",
        morada2: "Praia da Costa",
        codPostal: "29055-460 Vila Velha - ES",
        tel: "(27) 3319-8558",
        icon: "imagem/minibelabula.png",
        caminho: "imagem/miss.png"
    },
    /*pw brasil colatina*/
    {
        lat: -19.5213317,
        lng: -40.6330227,
        nome: "Pw Brasil Matriz Colatina",
        morada1: "Rua Manoel Milagres, 15",
        morada2: "Vila Kennedy",
        codPostal: "Baixo Guandu",
        tel: "(27) 2102-7450",
        icon: "imagem/minipw.png",
        caminho: "imagem/pw.png"
    },
    /*pw brasil baixo guandu*/
    {
        lat: -19.5150499,
        lng: -41.0075349,
        nome: "Pw Brasil Filial Baixo Guandu",
        morada1: "Rua dos BalneÃ¡rios do Complexo Desportivo",
        morada2: "Gafanha da NazarÃ©",
        codPostal: "3830-225 Gafanha da NazarÃ©",
        tel: "(27) 3319-8558",
        icon: "imagem/minipw.png",
        caminho: "imagem/pw.png"
    }
];
function initialize() {
    var mapOptions = {
        center: new google.maps.LatLng(40.601203, -8.668173),
        zoom: 9,
        mapTypeId: 'roadmap',
    };

    map = new google.maps.Map(document.getElementById('map'), mapOptions);

    infoWindow = new google.maps.InfoWindow();

    google.maps.event.addListener(map, 'click', function () {
        infoWindow.close();
    });

    displayMarkers();
}
google.maps.event.addDomListener(window, 'load', initialize);

function displayMarkers() {

    var bounds = new google.maps.LatLngBounds();

    for (var i = 0; i < markersData.length; i++) {

        var latlng = new google.maps.LatLng(markersData[i].lat, markersData[i].lng);
        var nome = markersData[i].nome;
        var morada1 = markersData[i].morada1;
        var morada2 = markersData[i].morada2;
        var codPostal = markersData[i].codPostal;
        var tel = markersData[i].tel;
        var icon = markersData[i].icon;
        var caminho = markersData[i].caminho;

        createMarker(latlng, nome, morada1, morada2, codPostal, tel, icon, caminho);

        bounds.extend(latlng);
    }

    map.fitBounds(bounds);
}

function createMarker(latlng, nome, morada1, morada2, codPostal, tel, icon, caminho) {
    var marker = new google.maps.Marker({
        map: map,
        position: latlng,
        title: nome,
        icon: icon
    });

    google.maps.event.addListener(marker, 'click', function () {

        var iwContent = '<img class="img-responsive" src="'+caminho+'" /><div id="iw_container">' +
                '<div class="iw_title">' + nome + '</div>' +
                morada2 + '<br />' +
                tel + '<br />' +
                '</div>';

        infoWindow.setContent(iwContent);

        infoWindow.open(map, marker);
    });
}