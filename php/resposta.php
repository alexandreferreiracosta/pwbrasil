<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>PW Brasil Export S/A</title>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800italic,800,700italic,700,600italic,400italic,600,300italic,300|Oswald:400,300,700' rel='stylesheet' type='text/css'>
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet">
        <link href="../css/owl.carousel.css" rel="stylesheet">
        <link href="../css/owl.theme.css" rel="stylesheet">
        <link href="..css/owl.transitions.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/index.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body style="background:#cccccc;">
        <div class="container-fluid" style="background:#0971b2; ">
            <center><img id="lg" class="img-responsive" style="padding: 10px;"></center>
        </div>
        <br>
        <div class="container-fluid">
            <center>
                <h1 style="color:#000000;">Seu curriculo foi enviada com sucesso!</h1>
                <p style="color:#000000;"> Aguarde a confirmação de recebimento no seu e-mail.</p>
                <a href="../index.php"><p style="color:#0971b2;">Voltar ao Inicio</p></a>
            </center>
        </div>
    </body>
</html>
