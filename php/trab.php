<!DOCTYPE html>
<html lang="PT">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>PW Brasil Export S/A</title>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800italic,800,700italic,700,600italic,400italic,600,300italic,300|Oswald:400,300,700' rel='stylesheet' type='text/css'>
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet">
        <link href="../css/owl.carousel.css" rel="stylesheet">
        <link href="../css/owl.theme.css" rel="stylesheet">
        <link href="../css/owl.transitions.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/index.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body style="background:#cccccc;">
 
    </body>
    <script src="js/validator.min.js"></script>
    <script>

        document.querySelector('#telefone').addEventListener('keypress', function (event) {
            var nome = $('#telefone').val();

            if (nome.length == 0) {
                nome += "(";
                $('#telefone').val(nome);
            } else if (nome.length == 4) {
                nome += ")";
                $('#telefone').val(nome);
            } else if (nome.length == 6) {
                nome += " ";
                $('#telefone').val(nome);
            } else if (nome.length == 11) {
                nome += "-";
                $('#telefone').val(nome);
            }
        });

        document.querySelector('#arq').addEventListener('click', function (event) {
            var nomearq;

            $('input:file').change(function () {
                var arq = this.files[0];
                nomearq = arq.name;

                if (nomearq != null) {
                    var values = nomearq.split('.');
                    if (values[values.length - 1] == "pdf" || values[values.length - 1] == "doc" || values[values.length - 1] == "docx") {
                        document.getElementById("arq").innerHTML = "Arquivo Selecionado: " + nomearq;
                        document.querySelector('.send').disabled = false;
                    } else {
                        document.querySelector('.send').disabled = true;
                        document.getElementById("arq").innerHTML = "Documento Invalido";
                    }
                }
            });
        });
    </script>
</html>
