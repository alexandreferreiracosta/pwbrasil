<!DOCTYPE html>
<html lang="PT">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>PW Brasil Export S/A</title>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800italic,800,700italic,700,600italic,400italic,600,300italic,300|Oswald:400,300,700' rel='stylesheet' type='text/css'>
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet">
        <link href="../css/owl.carousel.css" rel="stylesheet">
        <link href="../css/owl.theme.css" rel="stylesheet">
        <link href="../css/owl.transitions.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/index.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body style="background:#cccccc;">
        <div class="container-fluid" style="background:#0971b2; ">
            <h1 style="color:#FFF;">Trabalhe Conosco</h1>
        </div>
        <br>
        <div class="container-fluid">
            <form class="form-horizontal" data-toggle="validator" style="color:#000;" role="form" method="POST" action="envia_curriculo.php" enctype="multipart/form-data">
                <fieldset>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Nome Completo">Nome Completo</label>
                        <div class="col-md-4">
                            <input id="Nome Completo" name="Nome" type="text" placeholder="Nome Completo" class="form-control input-md">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="ar">Área de Atuação</label>
                        <div class="col-md-3">
                            <select id="UF" name="area" class="form-control">
                                <option value="Administrativo">Administrativo</option>
                                <option value="Produção">Produção</option>
                                <option value="Segurança">Segurança</option>
                                <option value="Serviços Gerais">Serviços Gerais</option>
                                <option value="Outros">Outros</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="ar">Unidade</label>
                        <div class="col-md-3">
                            <select id="UF" name="local" class="form-control">
                                <option value="Baixo Guandu">Baixo Guandu</option>
                                <option value="Colatina">Colatina</option>
                            </select>
                        </div>
                    </div>
                    
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="email">E-mail</label>
                        <div class="col-md-3">
                            <input id="email" name="email" type="email" placeholder="exemplo@exemplo.com" class="form-control input-md" required="">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Telefone Celular">Telefone</label>
                        <div class="col-md-2">
                            <input id="telefone" name="telefone" data-error="" type="tel" placeholder="(099)9 9999-9999" class="form-control input-md" maxlength="16">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <!-- File Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for=""></label>
                        <div class="col-md-4">
                            <label for='selecao-arquivo' id="arq" name="arq">Selecionar um arquivo &#187;</label>
                            <input id='selecao-arquivo' data-error="Por favor selecione um arquivo" type='file' name="arquivo" required="">
                            <p style="color:#d11010">Tipo do arquivo: doc, docx, pdf</p>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="singlebutton"></label>
                        <div class="col-md-4">
                            <button class="btn btn-main btn send" type="submit" id="send" data-loading-text="<i class='fa fa-spinner fa-spin'></i>"><i class="fa fa-paper-plane "></i>Enviar</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </body>

    <script src="js/validator.min.js"></script>
    <script>

        document.querySelector('#telefone').addEventListener('keypress', function (event) {
            var nome = $('#telefone').val();

            if (nome.length == 0) {
                nome += "(";
                $('#telefone').val(nome);
            } else if (nome.length == 4) {
                nome += ")";
                $('#telefone').val(nome);
            } else if (nome.length == 6) {
                nome += " ";
                $('#telefone').val(nome);
            } else if (nome.length == 11) {
                nome += "-";
                $('#telefone').val(nome);
            }
        });

        document.querySelector('#arq').addEventListener('click', function (event) {
            var nomearq;

            $('input:file').change(function () {
                var arq = this.files[0];
                nomearq = arq.name;

                if (nomearq != null) {
                    var values = nomearq.split('.');
                    if (values[values.length - 1] == "pdf" || values[values.length - 1] == "doc" || values[values.length - 1] == "docx") {
                        document.getElementById("arq").innerHTML = "Arquivo Selecionado: " + nomearq;
                        document.querySelector('.send').disabled = false;
                    } else {
                        document.querySelector('.send').disabled = true;
                        document.getElementById("arq").innerHTML = "Documento Invalido";
                    }
                }
            });
        });
    </script>
</html>
